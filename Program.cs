using System;
using SplashKitSDK;

namespace  piewgame
{
    public class Program
    {
        public static void Main()
        {
            //Create the window, player, and screen
            Window w = new Window("Piew Game", 1080, 720);
            Player player = new Player( new double[] {200,200}, new double[] {0,0});
            Screen screen = new Screen();

            //Generate the first level
            int level = 1;
            screen.GenerateLevel(level);
            screen.DisplayMessage(w, "Welcome to Piew. Your objective: Keep your friend safe, by keeping him on screen, and destroy all the comets.");
            screen.DisplayMessage(w, "Level: 1");

            // Basic event loop
            while(! w.CloseRequested)
            {
                //Check for keypresses
                if ( SplashKit.KeyDown(KeyCode.UpKey) && SplashKit.KeyDown(KeyCode.SpaceKey) )
                {
                    player.Velocity[0] -= 0.05*Math.Cos(player.Rotation * (Math.PI) / 180);
                    player.Velocity[1] -= 0.05*Math.Sin(player.Rotation * (Math.PI) / 180);
                    player.PlayerState = Player.PlayerStates.ShootFlying;
                    screen.AddBullet(player);
                }
                else if ( SplashKit.KeyDown(KeyCode.UpKey) )
                {
                    player.Velocity[0] -= 0.05*Math.Cos(player.Rotation * (Math.PI) / 180);
                    player.Velocity[1] -= 0.05*Math.Sin(player.Rotation * (Math.PI) / 180);
                    player.PlayerState = Player.PlayerStates.Flying;
                }
                else if ( SplashKit.KeyDown(KeyCode.SpaceKey) )
                {
                    player.PlayerState = Player.PlayerStates.Shooting;
                    screen.AddBullet(player);
                }
                else
                {
                    player.PlayerState = Player.PlayerStates.Idle;
                }

                if ( SplashKit.KeyDown(KeyCode.LeftKey) && !SplashKit.KeyDown(KeyCode.RightKey) )
                {
                    player.Rotation -= 3;
                    if (player.Rotation < 0)
                    {
                        player.Rotation += 360;
                    }
                }
                else if ( SplashKit.KeyDown(KeyCode.RightKey) && !SplashKit.KeyDown(KeyCode.LeftKey) )
                {
                    player.Rotation += 3;
                    if (player.Rotation > 360)
                    {
                        player.Rotation -= 360;
                    }

                }
                // Move all elements
                screen.Move(player);
                // Detect win/loose and crashes
                int gameState = screen.DetectCrash(player);
                if (gameState == -1) //Lose
                {
                    screen.DisplayMessage(w, "YOU LOSE                                   Score: " + level);
                    screen.DisplayMessage(w, "Play again?");
                    while (true)
                    {
                        w.Clear(Color.Black);
                        w.DrawText("Play again? Press Y to play again, N to exit", Color.White, "BoldFont" , 50, 100, 100);
                        // Refresh screen at target FPS
                        w.Refresh(60);
                        if ( SplashKit.KeyDown(KeyCode.NKey))
                        {
                            w.Close();
                            break;
                        } else if (SplashKit.KeyDown(KeyCode.YKey))
                        {
                            level = 1;
                            screen.GenerateLevel(level);
                            player = new Player( new double[] {200,200}, new double[] {0,0});
                            break;
                        }
                        SplashKit.ProcessEvents();
                    }
                } else if (gameState == 1) // Win
                {
                    level += 1;
                    screen.DisplayMessage(w, "WIN!                                       Next Level: " + level);
                    screen.GenerateLevel(level);
                    player = new Player( new double[] {200,200}, new double[] {0,0});

                }
                else //Game continues
                {
                    screen.Draw(w, player);
                }
                
            }
        }
    }
}

