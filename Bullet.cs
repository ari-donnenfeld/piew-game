using System;
using SplashKitSDK;

namespace piewgame
{
    public class Bullet : SpaceThing
    {
        // Bullet is a line, _x1 and _y1 are the end points of the bullet
        private double _x1;
        private double _y1;

        public Bullet(double[] position, double[] velocity) : base(position, velocity, 1, 1)
        {
            // Calculate the x and y positions of the start and endpoints of the bullet (relative to the velocity)
            // This will change the direction it is pointing (but not the direction it is heading)
            double omega = Math.Atan(velocity[1]/velocity[0]);
            int length = 6;
            _x1 = -Math.Cos(omega) * length/2;
            _y1 = Math.Sin(omega) * length/2;
        }

        public override void Draw(Window w)
        {
            // Draw the bullet onto the window
            w.DrawLine(Color.White, Position[0]-_x1, Position[1]+_y1, Position[0]+_x1, Position[1]-_y1, new DrawingOptions() {LineWidth = 2});
        }

        public override void Move()
        {
            //Move the bullet based on its velocity
            Position[0] += Velocity[0];
            Position[1] += Velocity[1];
        }
    }
}
