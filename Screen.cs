using System;
using System.Collections.Generic;
using SplashKitSDK;

namespace piewgame
{
    public class Screen
    {
        // Generate Lists for all space things except player
        private List<SpaceThing> _spaceThings;
        private List<Comet> _comets;
        private List<Bullet> _bullets;
        private Dumbo _dumbo;
        public Screen() {}

        private void Collide(SpaceThing spaceThing1, SpaceThing spacething2)
        {
            // Some very complex calculations for calculating aftermath of post elastic collision
            double dx = spacething2.Position[0] - spaceThing1.Position[0];
            double dy = spacething2.Position[1] - spaceThing1.Position[1];
            double angle = Math.Atan2(dy,dx);
            double sin = Math.Sin(angle);
            double cos = Math.Cos(angle);

            double x1 = 0, y1 = 0;
            double x2 = dx*cos+dy*sin;
            double y2 = dy*cos-dx*sin;

            double vx1 = spaceThing1.Velocity[0]*cos+spaceThing1.Velocity[1]*sin;
            double vy1 = spaceThing1.Velocity[1]*cos-spaceThing1.Velocity[0]*sin;
            double vx2 = spacething2.Velocity[0]*cos+spacething2.Velocity[1]*sin;
            double vy2 = spacething2.Velocity[1]*cos-spacething2.Velocity[0]*sin;
            

            double vx1final = ((spaceThing1.Mass-spacething2.Mass)*vx1+2*spacething2.Mass*vx2)/(spaceThing1.Mass+spacething2.Mass);
            double vx2final = ((spacething2.Mass-spaceThing1.Mass)*vx2+2*spaceThing1.Mass*vx1)/(spaceThing1.Mass+spacething2.Mass);
            vx1 = vx1final;
            vx2 = vx2final;

            double absV = Math.Abs(vx1)+Math.Abs(vx2);
            double overlap = (spaceThing1.Radius+spacething2.Radius)-Math.Abs(x1-x2);
            x1 += vx1/absV*overlap;
            x2 += vx2/absV*overlap;

            double x1final = x1*cos-y1*sin;
            double y1final = y1*cos+x1*sin;
            double x2final = x2*cos-y2*sin;
            double y2final = y2*cos+x2*sin;

            spacething2.Position[0] = spaceThing1.Position[0] + x2final;
            spacething2.Position[1] = spaceThing1.Position[1] + y2final;

            spaceThing1.Position[0] = spaceThing1.Position[0] + x1final;
            spaceThing1.Position[1] = spaceThing1.Position[1] + y1final;
            
            spaceThing1.Velocity[0] = vx1*cos-vy1*sin;
            spaceThing1.Velocity[1] = vy1*cos+vx1*sin;
            spacething2.Velocity[0] = vx2*cos-vy2*sin;
            spacething2.Velocity[1] = vy2*cos+vx2*sin;
        }

        public int DetectCrash(Player player)
        {
            // Player against comets
            foreach (Comet comet in _comets)
            {
                if (comet.DetectCrash(player.Position, player.Radius))
                {
                    Collide(player, comet);
                }
            }

            // Player against dumbo
            if (_dumbo.DetectCrash(player.Position, player.Radius))
            {
                Collide(player, _dumbo);
            
            }

            // Dumbo against comets
            foreach (Comet comet in _comets)
            {
                //Comets against Dumbo
                if (comet.DetectCrash(_dumbo.Position, _dumbo.Radius))
                {
                    Collide(_dumbo, comet);
                }
            }

            // Generate Lists for removal
            List<Bullet> bulletsRemove = new List<Bullet>();
            List<Comet> commetsRemove = new List<Comet>();
            List<Comet> commetsAdd = new List<Comet>();

            foreach (Bullet bullet in _bullets)
            {
                // Dumbo against bullet
                if (bullet.DetectCrash(_dumbo.Position, _dumbo.Radius))
                {
                    // This should move the dumob slightly, and destroy the bullet
                    Collide(_dumbo, bullet);
                    bulletsRemove.Add(bullet);
                }
                
                //Bullet against commet
                foreach (Comet comet in _comets)
                {
                    if (bullet.DetectCrash(comet.Position, comet.Radius))
                    {
                        // If its a big comet, then it will split, otherwise it should be destroyed
                        if (comet.Mass > 10)
                        {
                            //We are swapping the velocity x and y so they come out perpendicular
                            Comet newComet = new Comet(new [] {comet.Position[0]+comet.Radius/2, comet.Position[1]}, new [] {comet.Velocity[1], comet.Velocity[0]}, comet.Mass-5, 2);
                            Comet newComet2 = new Comet(new [] {comet.Position[0]-comet.Radius/2, comet.Position[1]}, new [] {-comet.Velocity[1], -comet.Velocity[0]}, comet.Mass-6, -2);
                            commetsAdd.Add(newComet);
                            commetsAdd.Add(newComet2);
                        }
                        commetsRemove.Add(comet);
                        bulletsRemove.Add(bullet);
                    }
                }
            }
            
            
            //Bullet against edge of screen, Garbage collector should remove it.
            foreach (Bullet bullet in _bullets)
            {
                if (bullet.Position[0] < -20) { bulletsRemove.Add(bullet); }
                if (bullet.Position[0] > 1100) { bulletsRemove.Add(bullet); }
                if (bullet.Position[1] < -20) { bulletsRemove.Add(bullet); }
                if (bullet.Position[1] > 730) { bulletsRemove.Add(bullet); }
            }
            foreach (Bullet bullet in bulletsRemove) //Actually remove the bullets
            {
                _bullets.Remove(bullet);
                _spaceThings.Remove(bullet);
            }
            foreach (Comet comet in commetsRemove) //Actually remove the comets
            {
                _comets.Remove(comet);
                _spaceThings.Remove(comet);
            }
            foreach (Comet comet in commetsAdd) //Actually add the comets
            {
                _comets.Add(comet);
                _spaceThings.Add(comet);
            }
            
            //Check for win (No more comets)
            if (_comets.Count == 0)
            {
                return 1;
            }
            
            // Check for lose (Dumbo reaches end of map)
            if (_dumbo.Position[0] < -20) { return -1; }
            if (_dumbo.Position[0] > 1100) { return -1; }
            if (_dumbo.Position[1] < -20) { return -1; }
            if (_dumbo.Position[1] > 730) { return -1; }

            //Otherwise return Ongoing Game
            return 0;
        }

        public void Move(Player player)
        {
            // Move all spacethings
            foreach (SpaceThing spaceThing in _spaceThings)
            {
                spaceThing.Move();
            }
            // and also the player
            player.Move();
        }

        public void Draw(Window w, Player player)
        {
            // Clear screen and set background color:
            w.Clear(Color.Black);     
            
            // Draw the player
            player.Draw(w);

            // Draw all other space things
            foreach (SpaceThing spaceThing in _spaceThings)
            {
                spaceThing.Draw(w);
            }
            
            // Refresh screen at target FPS
            w.Refresh(60);

            // Process SplashKit Events
            SplashKit.ProcessEvents();
        }

        // Initialise th bullet time count
        private int _bulletCount = 0;
        public void AddBullet(Player player)
        {
            // The player can only shoot 5 bullets per second.
            if (_bulletCount % 12 == 0)
            {
                //Some math for calculating the velocity of the bullet
                Bullet bullet = new Bullet(new double[] {player.Position[0], player.Position[1]}, new double[] {-10*Math.Cos(player.Rotation*Math.PI/180),-10*Math.Sin(player.Rotation*Math.PI/180)});
                _spaceThings.Add(bullet);
                _bullets.Add(bullet);
            }
            _bulletCount += 1;
        }

        public void DisplayMessage(Window w, string message)
        {
            // Create a typing text effect
            for (int i = 1; i < message.Length; i++)
            {
                // Clear screen and set background color:
                w.Clear(Color.Black);
                w.DrawText(message.Substring(0,i), Color.White, "BoldFont" , 50, 100, 100);
                // Refresh screen at target FPS
                w.Refresh(60);
                System.Threading.Thread.Sleep(50);
            }

            //Display full message for a few seconds
            w.DrawText(message, Color.White, "BoldFont" , 50, 100, 100);
            // Refresh screen at target FPS
            w.Refresh(60);
            SplashKit.ProcessEvents();
            System.Threading.Thread.Sleep(3000);

        }
        public void GenerateLevel(int level)
        {
            //Reset all the values
            _spaceThings = new List<SpaceThing>();
            _comets = new List<Comet>();
            _bullets = new List<Bullet>();
            _dumbo = new Dumbo(new double[] {540, 360}, new double[] {0, 0});
            _spaceThings.Add(_dumbo);
            // rnd used to make randomness in levels
            Random rnd = new Random();
            //Generate comets in random positoins, alternating the edge of screen to come from
            for (int i = 0; i < level+1; i++)
            {
                Comet comet;
                //Come from the top
                if (i % 4 == 0)
                {
                    comet = new Comet(new double[] {rnd.Next(50, 1000), -20}, new double[] {rnd.Next(-5, 5), rnd.Next(1, 5)}, rnd.Next(6, 17), rnd.Next(-2, 2));
                }
                //Come from the bottom
                else if (i % 4 == 1)
                {
                    comet = new Comet(new double[] {rnd.Next(50, 1000), 1100}, new double[] {rnd.Next(-5, 5), rnd.Next(-5, -1)}, rnd.Next(6, 17), rnd.Next(-2, 2));
                }
                //Come from the left
                else if (i % 4 == 2)
                {
                    comet = new Comet(new double[] {-20, rnd.Next(100, 620)}, new double[] {rnd.Next(1, 5), rnd.Next(-5, 5)}, rnd.Next(6, 17), rnd.Next(-2, 2));
                }
                //Come from the right
                else
                {
                    comet = new Comet(new double[] {740, rnd.Next(100, 620)}, new double[] {rnd.Next(-5, -1), rnd.Next(-5, 5)},  rnd.Next(6, 17), rnd.Next(-2, 2));
                }
                _spaceThings.Add(comet);
                _comets.Add(comet);
            }
        }
    }
}
