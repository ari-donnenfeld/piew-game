using System;
using SplashKitSDK;

namespace piewgame
{
    public class Player : SpaceThing
    {
        // Initialise the Animation and its options
        private AnimationScript _playerShipScript;
        private Animation _playerAnimation;
        private Bitmap _playerShip;
        private DrawingOptions _opt;
        private int _rotation;

        // Create enumaration for the different Animations/States
        public enum PlayerStates
        {
            Idle,
            Flying,
            Shooting,
            ShootFlying
        }
        private PlayerStates _playerState;

        public Player(double[] position, double[] velocity) : base(position, velocity, 35, 10)
        {
            //Load the bitmap and give it the details for the cells
            _playerShip = SplashKit.LoadBitmap("PlayerBmp","Player.png");
            _playerShip.SetCellDetails(471, 710, 4, 3, 12); // cell width, height, cols, rows, count
            //File containing the animation directions
            _playerShipScript = SplashKit.LoadAnimationScript("PlayerShipScript", "Player.txt");
            

            // Create the animation
            _playerAnimation = _playerShipScript.CreateAnimation("Flying");
        
            // Create a drawing option
            _opt = SplashKit.OptionWithAnimation(_playerAnimation);
            _opt.ScaleX = 0.15f;
            _opt.ScaleY = 0.15f;
            _rotation = 90;
            _playerState = PlayerStates.Idle;
        }

        public override void Move()
        {
            // Move the player based on its velocity
            Position[0] += Velocity[0];
            Position[1] += Velocity[1];
            
            // Loop around the map:
            if (Position[0] > 1090) { Position[0] = -5; }
            else if (Position[0] < -10) { Position[0] = 1085; }
            if (Position[1] > 730) { Position[1] = -5; }
            else if (Position[1] < -10) { Position[1] = 725; }
        }

        public int Rotation
        {
            get => _rotation;
            set => _rotation = value;
        }

        //Allow the animation to change
        public PlayerStates PlayerState
        {
            set => _playerState = value;
        }
        
        public override void Draw(Window w)
        {
            //First we check which state to use
            // Then we check if the animation has ended or has just changed into the new one
            // Assign should only be called once the animation is finished, or we are changing animations
            if (_playerState == PlayerStates.Flying)
            {
                if (_playerAnimation.Ended || _playerAnimation.Name != "flying")
                {
                    _playerAnimation.Assign("Flying");
                }
            }
            if (_playerState == PlayerStates.Shooting)
            {
                if (_playerAnimation.Ended || _playerAnimation.Name != "shooting")
                {
                    _playerAnimation.Assign("Shooting");
                }
            }
            if (_playerState == PlayerStates.ShootFlying)
            {
                if (_playerAnimation.Ended || _playerAnimation.Name != "shootflying")
                {
                    _playerAnimation.Assign("ShootFlying");
                }
            }
            if (_playerState == PlayerStates.Idle)
            {
                if (_playerAnimation.Ended || _playerAnimation.Name != "Idle")
                {
                    _playerAnimation.Assign("Idle");
                } 
            }
            // Update the frame of the animation
            _playerAnimation.Update();

            //We change the angle by -90 because we like our cartesian plane to have 0 degrees to the right, not at the top
            //Otherwise our formulas would be hard to calculate.
            _opt.Angle = Rotation-90;

            //Actually draw the frame.
            w.DrawBitmap(_playerShip, Position[0]-235, Position[1]-355, _opt);
        }
    }
}
