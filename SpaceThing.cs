using SplashKitSDK;

namespace piewgame
{
    // Abstract class used as building block for all visible objects in game.
    public abstract class SpaceThing
    {
        private double[] _position;
        private double[] _velocity;
        private int _radius;
        private int _mass;

        public SpaceThing(double[] position, double[] velocity, int radius, int mass)
        {
            _position = position;
            _velocity = velocity;
            _radius = radius;
            _mass = mass;
        }

        // Allow for view and edit of some variables
        public int Mass
        {
            get { return _mass; }
        }
        public double[] Velocity
        {
            get { return _velocity; }
            set { _velocity = value;  }
        }
        public double[] Position
        {
            get { return _position; }
            set { _position = value; }
        }
        public int Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }
        
        public bool DetectCrash(double[] position, int radius)
        {
            //Intead of using a square root to calculate the hypotinuse, we square the radius, so its quicker.
            return (position[0] - Position[0]) * (position[0] - Position[0]) +
                (position[1] - Position[1]) * (position[1] - Position[1]) < (radius + Radius) * (radius + Radius);
        }
        
        // Create abstract methods (to be overriden)
        public abstract void Draw(Window w);
        public abstract void Move();
        
    }
}
