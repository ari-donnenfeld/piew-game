using SplashKitSDK;

namespace piewgame
{
    public class Comet : SpaceThing
    {
        private Bitmap _cometBit;
        private int _angle;
        private int _rotation;
        
        public Comet(double[] position, double[] velocity, int mass, int rotation) : base(position, velocity, 28/9*mass+10/3, mass)
        {
            _cometBit = SplashKit.LoadBitmap("CometBmp","Comet.png");
            _angle = 0;
            //How much to rotate by
            _rotation = rotation;
        }

        public override void Draw(Window w)
        {
            /*
             *How to calculate these values:
             * A mass of 10 is a radius of 35 therefore:
             * 10/35=s/22
             * s = 6.3 (This is the mass of the small one)
             * 10/35=b/50
             * b = 14.2 (The mass of the big one)
             * Therefore:
             * Mass: 6-15
             * radius: 22-50
             * scale 0.3-0.7
             *
             * radius=m*mass+c
             * radius=28/9*mass+10/3
             * scale=m*mass+c
             * scale=0.04444*mass+1/30
             */
            w.DrawBitmap(_cometBit, Position[0]-70, Position[1]-70, new DrawingOptions() {ScaleX = 0.04444f*Mass+1/30f, ScaleY = 0.04444f*Mass+1/30f, Angle = _angle});
        }

        public override void Move()
        {
            //Move the bullet based on its velocity
            Position[0] += Velocity[0];
            Position[1] += Velocity[1];
            //rotate by the rotation
            _angle += _rotation;
            // Loop around the map:
            if (Position[0] > 1120) { Position[0] = -20; }
            else if (Position[0] < -20) { Position[0] = 1100; }
            if (Position[1] > 760) { Position[1] = -20; }
            else if (Position[1] < -20) { Position[1] = 740; }
        }
    }
}
