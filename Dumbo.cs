using System;
using SplashKitSDK;

namespace piewgame
{
    public class Dumbo : SpaceThing
    {
        //Initialise image
        private Bitmap _dumboShip;
        private int _rotation;

        public Dumbo(double[] position, double[] velocity) : base(position, velocity, 36, 10)
        {
            // Load the image
            _dumboShip = SplashKit.LoadBitmap("DumboBmp","Dumbo.png");
            _rotation = 90;
        }

        public override void Draw(Window w)
        {
            //Draw the image onto the screen
            w.DrawBitmap(_dumboShip, Position[0]-398, Position[1]-290, new DrawingOptions() {ScaleX = 0.15f, ScaleY = 0.15f, Angle = _rotation -90});
        }

        public override void Move()
        {
            //Move the dumbo based on its velocity
            Position[0] += Velocity[0];
            Position[1] += Velocity[1];
        }

    }
}
