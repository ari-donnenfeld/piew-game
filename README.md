
# PiewGame


This simple object-oriented, Asteroids like game was just a fun project I used to learn C#.

Your friend has lost power to his spaceship. Bad timing given that you are in the middle of an asteroid belt. The asteroids are knocking him around, keep him on screen by shooting at the asteroid, and bumping or shooting your friend back to the middle of the screen.

Creating this game is a great way to learn about Object Oriented Programming and its principles like; Abstraction, Polymorphism, Encapsulation and Inheritance.

This game is build with C# along with the .NET framework and SplashKit for graphics.

Here you can see the UML diagram, so that you can follow the code better:

![PiewGame UML](./Assets/UML_Diagram.png)

And the final result:

![Playing the game](./Assets/piewgame.gif)

